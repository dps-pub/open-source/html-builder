<?php namespace DPS\HtmlBuilder\Tests;

class TestCase extends \Orchestra\Testbench\TestCase
{
  protected function getPackageProviders($app)
  {
      return ['DPS\HtmlBuilder\ServiceProvider'];
  }
}